CREATE DATABASE tarea OWNER postgres;

CREATE TABLE alumnos (id_alum serial, cod_alum varchar(20) not null primary key,nombre_alum varchar(20));

INSERT INTO alumnos (cod_alum, nombre_alum) VALUES ('4130','cristian ramirez');
INSERT INTO alumnos (cod_alum, nombre_alum) VALUES ('4016','edgar rodriguez');
INSERT INTO alumnos (cod_alum, nombre_alum) VALUES ('3912','hernan perdomo');
INSERT INTO alumnos (cod_alum, nombre_alum) VALUES ('4210','cesar gualdron');
INSERT INTO alumnos (cod_alum, nombre_alum) VALUES ('4118','brayan perez');
INSERT INTO alumnos (cod_alum, nombre_alum) VALUES ('4006','camila gonzalez');

SELECT *FROM alumnos;
