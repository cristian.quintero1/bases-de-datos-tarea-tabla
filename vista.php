<?php
	include_once("conexion_post.php");
	$con=new conexion_db();
	$sql="SELECT *FROM alumnos";
	$data=$con->respuesta_consulta($sql);

?>

<!DOCTYPE>
<html lang="es">
<link rel="stylesheet" href="estilos.css">
<body>
	
	<div style="background-color:#5D6D7E" align="center"><b><font size="6" color="white" face="Comic Sans MS">TABLA ALUMNOS CON CONEXION POSTGRES</font></b></div>
	<br>
	<br>
	<div align="center">	
		<table border="1">
			<thead>
			<tr>
				<th> Id </th>
				<th> Codigo alumno</th>
				<th> Nombre alumno</th>
			</tr>
			</thead>
			<?php while ($row= pg_fetch_array($data)) { ?>
        	<tr>
          		<td><b><?php echo $row['id'] ?></b></td>
          		<td><?php echo $row['cod_alum'] ?></td>
          		<td><?php echo $row['nombre_alum'] ?></td>
        	</tr>
     		<?php } ?> 
		</table>
	</div>
	
</body>
</html>
